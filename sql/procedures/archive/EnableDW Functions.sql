CREATE OR REPLACE PROCEDURE enabledw.loadage(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.age (yearsage ,monthsage,weeksage,daysage ,createddate ,createdby ,modifieddate ,modifiedby )
SELECT EXTRACT(year FROM createddate ), EXTRACT(month FROM createddate ), EXTRACT(week FROM createddate ),
EXTRACT(day FROM createddate ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpatient s ;
END;





$$
;

CREATE OR REPLACE PROCEDURE enabledw.loaddate(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.date (yeardate ,monthdate ,quarterdate,weekdate,daydate ,createddate ,createdby ,modifieddate ,modifiedby )
SELECT DISTINCT EXTRACT(year FROM createddate ), EXTRACT(month FROM createddate ), EXTRACT(quarter FROM createddate ),EXTRACT(week FROM createddate ),
EXTRACT(day FROM createddate ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpatient s ;
END;






$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadgender(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.gender (gendertype,birthsextype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT gender, birthsex,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingpatient p; 
END;



$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadgeography(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
DECLARE
  error_val int;
BEGIN
	
  INSERT INTO enabledw.geography (city,county,state,postalcode,country ,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT city,'county', state, postalcode , country ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagepatient s; 
END;


$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadpracticetype(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.practicetype (practicetype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT practicetype ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagepractice pr; 
END;


$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadprovidertype(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.providertype (providertype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT providertype,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stageprovider p; 
END;


$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadsourcesystemname(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.sourcesystem (sourcesystemtype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT sourcesystemname,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagepracticegroup pg; 
END;


$$
;
