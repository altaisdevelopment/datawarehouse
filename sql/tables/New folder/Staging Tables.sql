-- staging.stagingpatient definition

-- Drop table

-- DROP TABLE staging.stagingpatient;

--DROP TABLE staging.stagingpatient;
CREATE TABLE IF NOT EXISTS staging.stagingpatient
(
	patientkey INTEGER NOT NULL  ENCODE az64
	,sourcesystemname VARCHAR(100)   ENCODE lzo
	,patientid INTEGER   ENCODE az64
	,pgid INTEGER   ENCODE az64
	,primarypracticeid INTEGER   ENCODE az64
	,registerpracticeid INTEGER   ENCODE az64
	,firstname VARCHAR(100)   ENCODE lzo
	,middlename VARCHAR(100)   ENCODE lzo
	,lastname VARCHAR(100)   ENCODE lzo
	,address1 VARCHAR(250)   ENCODE lzo
	,address2 VARCHAR(250)   ENCODE lzo
	,city VARCHAR(100)   ENCODE lzo
	,state VARCHAR(100)   ENCODE lzo
	,postalcode VARCHAR(25)   ENCODE lzo
	,county VARCHAR(100)   ENCODE lzo
	,country VARCHAR(100)   ENCODE lzo
	,email VARCHAR(300)   ENCODE lzo
	,homephone VARCHAR(25)   ENCODE lzo
	,workphone VARCHAR(25)   ENCODE lzo
	,mobilephone VARCHAR(25)   ENCODE lzo
	,gender VARCHAR(100)   ENCODE lzo
	,birthsex VARCHAR(50)   ENCODE lzo
	,dob VARCHAR(12)   ENCODE lzo
	,ssn VARCHAR(12)   ENCODE lzo
	,createddate TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT ('now'::character varying)::timestamp with time zone ENCODE az64
	,modifieddate TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT ('now'::character varying)::timestamp with time zone ENCODE az64
	,currentrecord BOOLEAN NOT NULL DEFAULT true ENCODE RAW
	,archiverecord BOOLEAN NOT NULL DEFAULT false ENCODE RAW
	,sourcepatientbusinesskey VARCHAR(256) NOT NULL  ENCODE lzo
	,sourcepatientrowkey VARCHAR(256)   ENCODE lzo
	,sourcepatientkey VARCHAR(1024)   ENCODE lzo
	,sourcepatientid VARCHAR(1024)   ENCODE lzo
	,sourcepracticegroupid VARCHAR(1024)   ENCODE lzo
	,genderidentity VARCHAR(1024)   ENCODE lzo
	,sourcepatienthashkey VARCHAR(1024)   ENCODE lzo
	,patientmatchhashkey VARCHAR(1024)   ENCODE lzo
	,PRIMARY KEY (patientkey)
)
DISTSTYLE AUTO
;
ALTER TABLE staging.stagingpatient owner to aqures01;


-- staging.stagingpractice definition

-- Drop table

-- DROP TABLE staging.stagingpractice;

--DROP TABLE staging.stagingpractice;
CREATE TABLE IF NOT EXISTS staging.stagingpractice
(
	practicekey INTEGER NOT NULL  ENCODE az64
	,sourcesystemname VARCHAR(100) NOT NULL  ENCODE lzo
	,practiceid INTEGER NOT NULL  ENCODE az64
	,pgid INTEGER   ENCODE az64
	,practicename VARCHAR(100)   ENCODE lzo
	,practicetype VARCHAR(100)   ENCODE lzo
	,address1 VARCHAR(250)   ENCODE lzo
	,address2 VARCHAR(250)   ENCODE lzo
	,city VARCHAR(100)   ENCODE lzo
	,state VARCHAR(100)   ENCODE lzo
	,postalcode VARCHAR(25)   ENCODE lzo
	,county VARCHAR(100)   ENCODE lzo
	,country VARCHAR(100)   ENCODE lzo
	,email VARCHAR(300)   ENCODE lzo
	,phone VARCHAR(25)   ENCODE lzo
	,fax VARCHAR(25)   ENCODE lzo
	,practiceurl VARCHAR(200)   ENCODE lzo
	,timezone VARCHAR(100)   ENCODE lzo
	,timezonename VARCHAR(100)   ENCODE lzo
	,providergroupname VARCHAR(100)   ENCODE lzo
	,hospitalpractice BOOLEAN NOT NULL DEFAULT false ENCODE RAW
	,createddate TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT ('now'::character varying)::timestamp with time zone ENCODE az64
	,modifieddate TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT ('now'::character varying)::timestamp with time zone ENCODE az64
	,currentrecord BOOLEAN NOT NULL DEFAULT true ENCODE RAW
	,archiverecord BOOLEAN NOT NULL DEFAULT false ENCODE RAW
	,sourcepracticebusinesskey VARCHAR(256) NOT NULL  ENCODE lzo
	,sourcepracticerowkey VARCHAR(256)   ENCODE lzo
	,sourcepracticekey VARCHAR(1024)   ENCODE lzo
	,sourcepgid VARCHAR(1024)   ENCODE lzo
	,sourcepracticeid VARCHAR(1024)   ENCODE lzo
	,sourcepracticehashkey VARCHAR(1024)   ENCODE lzo
	,matchpracticehashkey VARCHAR(1024)   ENCODE lzo
	,PRIMARY KEY (practicekey)
)
DISTSTYLE AUTO
;
ALTER TABLE staging.stagingpractice owner to aqures01;


-- staging.stagingpracticegroup definition

-- Drop table

-- DROP TABLE staging.stagingpracticegroup;

--DROP TABLE staging.stagingpracticegroup;
CREATE TABLE IF NOT EXISTS staging.stagingpracticegroup
(
	pgkey INTEGER NOT NULL  ENCODE az64
	,sourcesystemname VARCHAR(100) NOT NULL  ENCODE lzo
	,pgid INTEGER NOT NULL  ENCODE az64
	,practicegroupname VARCHAR(100)   ENCODE lzo
	,practicegroupurl VARCHAR(200)   ENCODE lzo
	,createddate TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT ('now'::character varying)::timestamp with time zone ENCODE az64
	,modifieddate TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT ('now'::character varying)::timestamp with time zone ENCODE az64
	,currentrecord BOOLEAN NOT NULL DEFAULT true ENCODE RAW
	,archiverecord BOOLEAN NOT NULL DEFAULT false ENCODE RAW
	,sourcepgbusinesskey VARCHAR(256) NOT NULL  ENCODE lzo
	,sourcepgrowkey VARCHAR(256)   ENCODE lzo
	,sourcepghashkey VARCHAR(1024)   ENCODE lzo
	,matchpghashkey VARCHAR(1024)   ENCODE lzo
	,PRIMARY KEY (pgkey)
)
DISTSTYLE AUTO
;
ALTER TABLE staging.stagingpracticegroup owner to aqures01;


-- staging.stagingprovider definition

-- Drop table

-- DROP TABLE staging.stagingprovider;

--DROP TABLE staging.stagingprovider;
CREATE TABLE IF NOT EXISTS staging.stagingprovider
(
	sourceproviderkey INTEGER   ENCODE az64
	,sourcesystemname VARCHAR(100)   ENCODE lzo
	,sourceproviderid INTEGER   ENCODE az64
	,pgid INTEGER   ENCODE az64
	,npi BIGINT   ENCODE az64
	,firstname VARCHAR(100)   ENCODE lzo
	,middlename VARCHAR(100)   ENCODE lzo
	,lastname VARCHAR(100)   ENCODE lzo
	,ssousername VARCHAR(100)   ENCODE lzo
	,address1 VARCHAR(250)   ENCODE lzo
	,address2 VARCHAR(250)   ENCODE lzo
	,city VARCHAR(100)   ENCODE lzo
	,state VARCHAR(100)   ENCODE lzo
	,postalcode VARCHAR(25)   ENCODE lzo
	,county VARCHAR(100)   ENCODE lzo
	,country VARCHAR(100)   ENCODE lzo
	,email VARCHAR(300)   ENCODE lzo
	,homephone VARCHAR(25)   ENCODE lzo
	,workphone VARCHAR(25)   ENCODE lzo
	,mobilephone VARCHAR(25)   ENCODE lzo
	,providertypeid VARCHAR(100)   ENCODE lzo
	,providertype VARCHAR(100)   ENCODE lzo
	,ansinamecode VARCHAR(200)   ENCODE lzo
	,createddate TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT ('now'::character varying)::timestamp with time zone ENCODE az64
	,modifieddate TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT ('now'::character varying)::timestamp with time zone ENCODE az64
	,currentrecord BOOLEAN NOT NULL DEFAULT true ENCODE RAW
	,archiverecord BOOLEAN NOT NULL DEFAULT false ENCODE RAW
	,sourceproviderbusinesskey VARCHAR(256) NOT NULL DEFAULT 0 ENCODE lzo
	,sourceproviderhashkey VARCHAR(256) NOT NULL DEFAULT 0 ENCODE lzo
	,sourceproviderrowkey VARCHAR(256) NOT NULL DEFAULT 0 ENCODE lzo
	,matchproviderbusinesskey VARCHAR(1024)   ENCODE lzo
	,matchproviderhashkey VARCHAR(1024)   ENCODE lzo
)
DISTSTYLE AUTO
;
ALTER TABLE staging.stagingprovider owner to aqures01;