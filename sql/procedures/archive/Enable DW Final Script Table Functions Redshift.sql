TRUNCATE TABLE enabledw.age;
TRUNCATE TABLE enabledw.date;
TRUNCATE TABLE enabledw.gender;
TRUNCATE TABLE enabledw.geography ;
TRUNCATE TABLE enabledw.practicetype ;
TRUNCATE TABLE enabledw.providertype ;
TRUNCATE TABLE enabledw.sourcesystem ;
TRUNCATE TABLE enabledw.enablepatientfacts;
TRUNCATE TABLE enabledw.enableproviderfacts;
TRUNCATE TABLE enabledw.enablepracticefacts;


CALL enabledw.loadage();
CALL enabledw.loaddate();
CALL enabledw.loadgender();
CALL enabledw.loadgeography();
CALL enabledw.loadpracticetype();
CALL enabledw.loadprovidertype();
CALL enabledw.loadsourcesystemname();
CALL enabledw.loadenablepatientfacts();
CALL enabledw.loadenableproviders();


SELECT * FROM enabledw.age;
SELECT * FROM enabledw.date;
SELECT * FROM enabledw.gender;
SELECT * FROM enabledw.geography ;
SELECT * FROM enabledw.practicetype ;
SELECT * FROM enabledw.providertype ;
SELECT * FROM enabledw.sourcesystem ;
SELECT * FROM enabledw.enablepatientfacts;
SELECT * FROM enabledw.enableproviderfacts;
SELECT * FROM enabledw.enablepracticefacts;

-- enabledw.age definition

-- Drop table

-- DROP TABLE enabledw.age;

--DROP TABLE enabledw.age;
CREATE TABLE IF NOT EXISTS enabledw.age
(
	ageid BIGINT NOT NULL DEFAULT "identity"(136887, 0, ('1,1'::character varying)::text) ENCODE az64
	,dob DATE   ENCODE az64
	,yearsage INTEGER   ENCODE az64
	,monthsage INTEGER   ENCODE az64
	,weeksage INTEGER   ENCODE az64
	,daysage INTEGER   ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (ageid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.age owner to aqures01;


-- enabledw."date" definition

-- Drop table

-- DROP TABLE enabledw."date";

--DROP TABLE enabledw.date;
CREATE TABLE IF NOT EXISTS enabledw.date
(
	dateid BIGINT NOT NULL DEFAULT "identity"(136893, 0, ('1,1'::character varying)::text) ENCODE az64
	,datefull DATE   ENCODE az64
	,yeardate INTEGER   ENCODE az64
	,monthdate INTEGER   ENCODE az64
	,quarterdate INTEGER   ENCODE az64
	,weekdate INTEGER   ENCODE az64
	,daydate INTEGER   ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (dateid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.date owner to aqures01;


-- enabledw.enablepatientfacts definition

-- Drop table

-- DROP TABLE enabledw.enablepatientfacts;

--DROP TABLE enabledw.enablepatientfacts;
CREATE TABLE IF NOT EXISTS enabledw.enablepatientfacts
(
	enablepatientfactid BIGINT NOT NULL DEFAULT "identity"(136921, 0, ('1,1'::character varying)::text) ENCODE az64
	,geographyid INTEGER NOT NULL  ENCODE az64
	,dateid INTEGER NOT NULL  ENCODE az64
	,genderid INTEGER NOT NULL  ENCODE az64
	,ageid INTEGER NOT NULL  ENCODE az64
	,totalpatients INTEGER NOT NULL  ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (enablepatientfactid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.enablepatientfacts owner to aqures01;


-- enabledw.enablepracticefacts definition

-- Drop table

-- DROP TABLE enabledw.enablepracticefacts;

--DROP TABLE enabledw.enablepracticefacts;
CREATE TABLE IF NOT EXISTS enabledw.enablepracticefacts
(
	enablepracticefactid BIGINT NOT NULL DEFAULT "identity"(136938, 0, ('1,1'::character varying)::text) ENCODE az64
	,geographyid INTEGER NOT NULL  ENCODE az64
	,practicetypeid INTEGER   ENCODE az64
	,dateid INTEGER NOT NULL  ENCODE az64
	,totalpractices INTEGER NOT NULL  ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (enablepracticefactid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.enablepracticefacts owner to aqures01;


-- enabledw.enableproviderfacts definition

-- Drop table

-- DROP TABLE enabledw.enableproviderfacts;

--DROP TABLE enabledw.enableproviderfacts;
CREATE TABLE IF NOT EXISTS enabledw.enableproviderfacts
(
	enableproviderfactid BIGINT NOT NULL DEFAULT "identity"(136911, 0, ('1,1'::character varying)::text) ENCODE az64
	,geographyid INTEGER NOT NULL  ENCODE az64
	,providertypeid INTEGER   ENCODE az64
	,dateid INTEGER NOT NULL  ENCODE az64
	,genderid INTEGER NOT NULL  ENCODE az64
	,ageid INTEGER NOT NULL  ENCODE az64
	,totalproviders INTEGER NOT NULL  ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (enableproviderfactid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.enableproviderfacts owner to aqures01;


-- enabledw.gender definition

-- Drop table

-- DROP TABLE enabledw.gender;

--DROP TABLE enabledw.gender;
CREATE TABLE IF NOT EXISTS enabledw.gender
(
	genderid INTEGER NOT NULL DEFAULT default_identity(118214, 0, ('1,1'::character varying)::text) ENCODE az64
	,gendertype VARCHAR(100)   ENCODE lzo
	,birthsextype VARCHAR(100)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (genderid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.gender owner to aqures01;


-- enabledw.geography definition

-- Drop table

-- DROP TABLE enabledw.geography;

--DROP TABLE enabledw.geography;
CREATE TABLE IF NOT EXISTS enabledw.geography
(
	geographyid BIGINT NOT NULL DEFAULT "identity"(136899, 0, ('1,1'::character varying)::text) ENCODE az64
	,city VARCHAR(40)   ENCODE lzo
	,county VARCHAR(40)   ENCODE lzo
	,state VARCHAR(40)   ENCODE lzo
	,country VARCHAR(40)   ENCODE lzo
	,postalcode VARCHAR(10)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (geographyid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.geography owner to aqures01;


-- enabledw.practicetype definition

-- Drop table

-- DROP TABLE enabledw.practicetype;

--DROP TABLE enabledw.practicetype;
CREATE TABLE IF NOT EXISTS enabledw.practicetype
(
	practicetypeid BIGINT NOT NULL DEFAULT "identity"(118165, 0, ('1,1'::character varying)::text) ENCODE az64
	,practicetype VARCHAR(100)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (practicetypeid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.practicetype owner to aqures01;


-- enabledw.providertype definition

-- Drop table

-- DROP TABLE enabledw.providertype;

--DROP TABLE enabledw.providertype;
CREATE TABLE IF NOT EXISTS enabledw.providertype
(
	providertypeid BIGINT NOT NULL DEFAULT "identity"(118160, 0, ('1,1'::character varying)::text) ENCODE az64
	,providertype VARCHAR(100)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (providertypeid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.providertype owner to aqures01;


-- enabledw.sourcesystem definition

-- Drop table

-- DROP TABLE enabledw.sourcesystem;

--DROP TABLE enabledw.sourcesystem;
CREATE TABLE IF NOT EXISTS enabledw.sourcesystem
(
	sourcesystemid BIGINT NOT NULL DEFAULT "identity"(118175, 0, ('1,1'::character varying)::text) ENCODE az64
	,sourcesystemtype VARCHAR(100)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (sourcesystemid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.sourcesystem owner to aqures01;

CREATE OR REPLACE PROCEDURE enabledw.loadage()
	LANGUAGE plpgsql
AS $$
	
	
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.age (dob,yearsage ,monthsage,weeksage,daysage ,createddate ,createdby ,modifieddate ,modifiedby )
SELECT TO_DATE(s.dob,'YYMDD'),EXTRACT(YEAR  FROM TO_DATE(s.dob,'YYMDD') ), EXTRACT(MONTH  FROM TO_DATE(s.dob,'YYMDD') ), EXTRACT(WEEK  FROM TO_DATE(s.dob,'YYMDD') ),
EXTRACT(DAY  FROM TO_DATE(s.dob,'YYMDD') ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpatient s ;
END;








$$
;

CREATE OR REPLACE PROCEDURE enabledw.loaddate()
	LANGUAGE plpgsql
AS $$
	
	
	
	
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.date (datefull,yeardate ,monthdate ,quarterdate,weekdate,daydate ,createddate ,createdby ,modifieddate ,modifiedby )
SELECT DISTINCT CAST(createddate AS DATE), EXTRACT(year FROM createddate ), EXTRACT(month FROM createddate ), EXTRACT(quarter FROM createddate ),EXTRACT(week FROM createddate ),
EXTRACT(day FROM createddate ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpatient s 
UNION
SELECT DISTINCT CAST(createddate AS DATE), EXTRACT(year FROM createddate ), EXTRACT(month FROM createddate ), EXTRACT(quarter FROM createddate ),EXTRACT(week FROM createddate ),
EXTRACT(day FROM createddate ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingprovider p
UNION
SELECT DISTINCT CAST(createddate AS DATE), EXTRACT(year FROM createddate ), EXTRACT(month FROM createddate ), EXTRACT(quarter FROM createddate ),EXTRACT(week FROM createddate ),
EXTRACT(day FROM createddate ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpractice pr 
UNION
SELECT DISTINCT CAST(createddate AS DATE), EXTRACT(year FROM createddate ), EXTRACT(month FROM createddate ), EXTRACT(quarter FROM createddate ),EXTRACT(week FROM createddate ),
EXTRACT(day FROM createddate ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpracticegroup pg
;
END;


--SELECT DISTINCT CAST(createddate AS DATE) FROM staging.stagingprovider;








$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadenablepatientfacts()
	LANGUAGE plpgsql
AS $$
	
	
	
	
	

DECLARE
  error_val int;
BEGIN
 
  INSERT INTO enabledw.enablepatientfacts (totalpatients,geographyid,genderid,dateid,ageid,createddate ,createdby ,modifieddate ,modifiedby )
   -- sum of fact quant measures
   -- , ageid 
SELECT COUNT(S.patientid) AS PatientCount,

             ISNULL(G.geographyid, -1) AS GeographyID,

             ISNULL(GN.GenderID, -1) AS GenderID,

             ISNULL(D.DateID, -1) AS DateID,
			
             ISNULL(a.ageid, -1) AS AgeID,
CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'                          

       FROM staging.StagingPatient S

             LEFT JOIN enabledw.Geography G

                    ON S.country = G.country

                           AND S.state = G.state

                          AND S.city = G.city

                          AND S.postalcode = G.postalcode

             LEFT JOIN enabledw.Gender GN

                    ON S.gender = GN.gendertype
			LEFT JOIN enabledw.age a
-- join on the actual date and the createddate/timestamp remove for the date
                    ON EXTRACT(YEAR  FROM TO_DATE(s.dob,'YYMDD') ) = a.yearsage

                    AND EXTRACT(MONTH FROM TO_DATE(s.dob,'YYMDD')) = a.monthsage

                    AND EXTRACT(DAY FROM TO_DATE(s.dob,'YYMDD') ) = a.daysage
                  
                      AND EXTRACT(WEEK FROM TO_DATE(s.dob,'YYMDD') ) = a.weeksage

                    
                    
             LEFT JOIN enabledw.date d 

                    ON EXTRACT(YEAR  FROM s.createddate) = d.yeardate

                    AND EXTRACT(MONTH FROM s.createddate) = d.monthdate

                    AND EXTRACT(DAY FROM s.createddate) = d.daydate

       GROUP BY G.geographyid, GN.GenderID, D.DateID,a.ageid;
END;





$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadenablepractices()
	LANGUAGE plpgsql
AS $$
	
	
	
	
	
	

DECLARE
  error_val int;
BEGIN
INSERT INTO enabledw.enablepracticefacts (totalpractices,geographyid,dateid,practicetypeid ,createddate ,createdby ,modifieddate ,modifiedby )
   -- sum of fact quant measures
   -- , ageid 
  -- PRACTICE FACT
  SELECT COUNT(P.practiceid) AS PracticeCount, 
  	ISNULL(G.geographyid, -1) AS GeographyID,
 
                        ISNULL(D.DateID, -1) AS DateID,

  	ISNULL(PT.practicetypeid) AS PracticeTypeID,
  	CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
  FROM staging.StagingPractice P
  	LEFT JOIN enabledw.geography G 
  		ON P.country = G.country 
  			 AND P.state = G.state
  			AND P.city = G.city 
  			AND P.postalcode  = G.postalcode 


              LEFT JOIN enabledw.Date d

                    ON EXTRACT(YEAR  FROM p.createddate) = d.yeardate

                    AND EXTRACT(MONTH FROM p.createddate) = d.monthdate

                    AND EXTRACT(DAY FROM p.createddate) = d.daydate

  -- taking out this for now until we decide to get this type in the table                    
 	LEFT JOIN enabledw.practiceType PT 
  		ON P.practicetype = PT.practicetype
  GROUP BY G.geographyid, PT.practicetypeid,d.dateid; 
    
END;






$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadenableproviders()
	LANGUAGE plpgsql
AS $$
	
	
	
	
	

DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.enableproviderfacts (totalproviders,geographyid,genderid,ageid, dateid,providertypeid ,createddate ,createdby ,modifieddate ,modifiedby )
   -- sum of fact quant measures
   -- , ageid 
  -- PROVIDER FACT
  SELECT COUNT(P.sourceproviderid) AS ProviderCount, 
  	ISNULL(G.geographyid, -1) AS GeographyID, 
           -1 AS GenderID, --  ISNULL(GN.GenderID, -1) AS GenderID,
-1 as AgeID, -- age id
             ISNULL(D.DateID, -1) AS DateID --,
			
           -- -1 -- ISNULL(a.ageid, -1) AS AgeID
  	,ISNULL(PT.providertypeid) AS ProviderTypeID,
  	CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
  	
  FROM staging.StagingProvider P
  	LEFT JOIN enabledw.geography g  
  		ON P.country = G.country 
  			 AND P.state = G.state
  			AND P.city = G.city 
  			AND P.postalcode  = G.postalcode 
  			
  		--	  	   LEFT JOIN Gender GN

          --          ON S.gender = GN.gendertype		
  	--		LEFT JOIN age a
-- join on the actual date and the createddate/timestamp remove for the date
      --              ON EXTRACT(YEAR  FROM TO_DATE(s.dob,'YYMDD') ) = a.yearsage

       --             AND EXTRACT(MONTH FROM TO_DATE(s.dob,'YYMDD')) = a.monthsage

         --           AND EXTRACT(DAY FROM TO_DATE(s.dob,'YYMDD') ) = a.daysage
        --          
         --             AND EXTRACT(WEEK FROM TO_DATE(s.dob,'YYMDD') ) = a.weeksage

              LEFT JOIN enabledw.date d

                    ON EXTRACT(YEAR  FROM P.createddate) = d.yeardate

                    AND EXTRACT(MONTH FROM P.createddate) = d.monthdate

                    AND EXTRACT(DAY FROM P.createddate) = d.daydate
  	LEFT JOIN enabledw.providertype pt 
  	 	ON P.providertype = PT.providertype
  GROUP BY G.geographyid, PT.providertypeid,d.dateid; 
  
END;







$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadgender()
	LANGUAGE plpgsql
AS $$
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.gender (gendertype,birthsextype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT gender, birthsex,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingpatient p; 
END;




$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadgeography()
	LANGUAGE plpgsql
AS $$
	
	
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.geography (city,county,state,postalcode,country ,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT city,'NA',state,postalcode , country ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingpatient s
	UNION
 SELECT DISTINCT city,'NA',state,postalcode , country ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingprovider p
	UNION
 SELECT DISTINCT city,'NA',state,postalcode , country ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingpractice pr; 
END;






$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadpracticetype()
	LANGUAGE plpgsql
AS $$
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.practicetype (practicetype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT practicetype ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingpractice pr; 
END;




$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadprovidertype()
	LANGUAGE plpgsql
AS $$
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.providertype (providertype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT providertype,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingprovider p; 
END;




$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadsourcesystemname()
	LANGUAGE plpgsql
AS $$
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.sourcesystem (sourcesystemtype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT sourcesystemname,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingpracticegroup pg; 
END;




$$
;
