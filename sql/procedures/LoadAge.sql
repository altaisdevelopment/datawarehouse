

CREATE OR REPLACE PROCEDURE enabledw.LoadAge()
	LANGUAGE plpgsql
AS $$
	
DECLARE
  error_val int;
BEGIN
	INSERT INTO enabledw.age (dob, yearsage, monthsage, weeksage, daysage, createddate, createdby, modifieddate, modifiedby )
	SELECT DISTINCT 
		CAST(s.dob AS DATE), 
		EXTRACT(YEAR  FROM TO_DATE(s.dob,'YYMDD') ), 
		EXTRACT(MONTH  FROM TO_DATE(s.dob,'YYMDD') ), 
		EXTRACT(WEEK  FROM TO_DATE(s.dob,'YYMDD') ), 
		EXTRACT(DAY  FROM TO_DATE(s.dob,'YYMDD') ), 
		CURRENT_TIMESTAMP , 
		'System', 
		CURRENT_TIMESTAMP , 
		'System'
	FROM staging.stagingpatients;
END;
$$
;