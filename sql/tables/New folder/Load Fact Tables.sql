CREATE OR REPLACE PROCEDURE enabledw.loadenablepatientfacts(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	

DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.enablepatientfacts (totalpatients,geographyid,genderid,dateid,createddate ,createdby ,modifieddate ,modifiedby )
   -- sum of fact quant measures
   -- , ageid 
SELECT COUNT(S.patientid) AS PatientCount,

             ISNULL(G.geographyid, -1) AS GeographyID,

             ISNULL(GN.GenderID, -1) AS GenderID,

             ISNULL(D.DateID, -1) AS DateID

       FROM staging.StagingPatient S

             LEFT JOIN Geography G

                    ON S.country = G.country

                          -- AND S.state = G.state

                          AND S.city = G.city

                          AND S.postalcode = G.postalcode

             LEFT JOIN Gender GN

                    ON S.gender = GN.gendertype

             LEFT JOIN Date D

                    ON EXTRACT(YEAR  FROM s.createddate) = d.yeardate

                    AND EXTRACT(MONTH FROM s.createddate) = d.monthdate

                    AND EXTRACT(DAY FROM s.createddate) = d.daydate

       GROUP BY G.geographyid, GN.GenderID, D.DateID;
END;

$$
;
