CREATE TABLE IF NOT EXISTS enabledw.geography
(
	geographyid BIGINT NOT NULL DEFAULT "identity"(136899, 0, ('1,1'::character varying)::text) ENCODE az64
	,city VARCHAR(40)   ENCODE lzo
	,county VARCHAR(40)   ENCODE lzo
	,state VARCHAR(40)   ENCODE lzo
	,country VARCHAR(40)   ENCODE lzo
	,postalcode VARCHAR(10)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (geographyid)
);
