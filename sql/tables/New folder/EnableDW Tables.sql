-- enabledw.age definition

-- Drop table

-- DROP TABLE enabledw.age;

--DROP TABLE enabledw.age;
CREATE TABLE IF NOT EXISTS enabledw.age
(
	ageid BIGINT NOT NULL DEFAULT "identity"(118189, 0, ('1,1'::character varying)::text) ENCODE az64
	,yearsage INTEGER   ENCODE az64
	,monthsage INTEGER   ENCODE az64
	,weeksage INTEGER   ENCODE az64
	,daysage INTEGER   ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (ageid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.age owner to aqures01;


-- enabledw."date" definition

-- Drop table

-- DROP TABLE enabledw."date";

--DROP TABLE enabledw.date;
CREATE TABLE IF NOT EXISTS enabledw.date
(
	dateid BIGINT NOT NULL DEFAULT "identity"(118184, 0, ('1,1'::character varying)::text) ENCODE az64
	,yeardate INTEGER   ENCODE az64
	,monthdate INTEGER   ENCODE az64
	,quarterdate INTEGER   ENCODE az64
	,weekdate INTEGER   ENCODE az64
	,daydate INTEGER   ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (dateid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.date owner to aqures01;


-- enabledw.enablefacts definition

-- Drop table

-- DROP TABLE enabledw.enablefacts;

--DROP TABLE enabledw.enablefacts;
CREATE TABLE IF NOT EXISTS enabledw.enablefacts
(
	enablefactid BIGINT NOT NULL DEFAULT "identity"(118220, 0, '1,1'::text) ENCODE az64
	,geographyid INTEGER NOT NULL  ENCODE az64
	,dateid INTEGER NOT NULL  ENCODE az64
	,patientid INTEGER NOT NULL  ENCODE az64
	,practicetypeid INTEGER NOT NULL  ENCODE az64
	,genderid INTEGER NOT NULL  ENCODE az64
	,ageid INTEGER NOT NULL  ENCODE az64
	,patietcount INTEGER NOT NULL  ENCODE az64
	,practicetypecount INTEGER NOT NULL  ENCODE az64
	,providertypecount INTEGER NOT NULL  ENCODE az64
	,geographycount INTEGER NOT NULL  ENCODE az64
	,agecount INTEGER NOT NULL  ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (enablefactid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.enablefacts owner to aqures01;

--DROP TABLE enabledw.enablepatientfacts;
CREATE TABLE IF NOT EXISTS enabledw.enablepatientfacts
(
	enablepatientfactid BIGINT NOT NULL DEFAULT "identity"(118318, 0, ('1,1'::character varying)::text) ENCODE az64
	,geographyid INTEGER NOT NULL  ENCODE az64
	,dateid INTEGER NOT NULL  ENCODE az64
	,genderid INTEGER NOT NULL  ENCODE az64
	,ageid INTEGER NOT NULL  ENCODE az64
	,totalpatients INTEGER NOT NULL  ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (enablepatientfactid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.enablepatientfacts owner to aqures01;


-- enabledw.enablepatientfacts definition

-- Drop table

-- DROP TABLE enabledw.enablepatientfacts;

--DROP TABLE enabledw.enablepatientfacts;
CREATE TABLE IF NOT EXISTS enabledw.enablepatientfacts
(
	enablepatientfactid BIGINT NOT NULL DEFAULT "identity"(118235, 0, '1,1'::text) ENCODE az64
	,geographyid INTEGER NOT NULL  ENCODE az64
	,dateid INTEGER NOT NULL  ENCODE az64
	,patientid INTEGER NOT NULL  ENCODE az64
	,genderid INTEGER NOT NULL  ENCODE az64
	,ageid INTEGER NOT NULL  ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (enablepatientfactid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.enablepatientfacts owner to aqures01;


-- enabledw.gender definition

-- Drop table

-- DROP TABLE enabledw.gender;

--DROP TABLE enabledw.gender;
CREATE TABLE IF NOT EXISTS enabledw.gender
(
	genderid INTEGER NOT NULL DEFAULT default_identity(118214, 0, ('1,1'::character varying)::text) ENCODE az64
	,gendertype VARCHAR(100)   ENCODE lzo
	,birthsextype VARCHAR(100)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (genderid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.gender owner to aqures01;


-- enabledw.geography definition

-- Drop table

-- DROP TABLE enabledw.geography;

--DROP TABLE enabledw.geography;
CREATE TABLE IF NOT EXISTS enabledw.geography
(
	geographyid BIGINT NOT NULL DEFAULT "identity"(118170, 0, ('1,1'::character varying)::text) ENCODE az64
	,city VARCHAR(40)   ENCODE lzo
	,county VARCHAR(40)   ENCODE lzo
	,state VARCHAR(40)   ENCODE lzo
	,country VARCHAR(40)   ENCODE lzo
	,postalcode VARCHAR(10)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (geographyid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.geography owner to aqures01;


-- enabledw.practicetype definition

-- Drop table

-- DROP TABLE enabledw.practicetype;

--DROP TABLE enabledw.practicetype;
CREATE TABLE IF NOT EXISTS enabledw.practicetype
(
	practicetypeid BIGINT NOT NULL DEFAULT "identity"(118165, 0, ('1,1'::character varying)::text) ENCODE az64
	,practicetype VARCHAR(100)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (practicetypeid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.practicetype owner to aqures01;


-- enabledw.providertype definition

-- Drop table

-- DROP TABLE enabledw.providertype;

--DROP TABLE enabledw.providertype;
CREATE TABLE IF NOT EXISTS enabledw.providertype
(
	providertypeid BIGINT NOT NULL DEFAULT "identity"(118160, 0, ('1,1'::character varying)::text) ENCODE az64
	,providertype VARCHAR(100)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (providertypeid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.providertype owner to aqures01;


-- enabledw.sourcesystem definition

-- Drop table

-- DROP TABLE enabledw.sourcesystem;

--DROP TABLE enabledw.sourcesystem;
CREATE TABLE IF NOT EXISTS enabledw.sourcesystem
(
	sourcesystemid BIGINT NOT NULL DEFAULT "identity"(118175, 0, ('1,1'::character varying)::text) ENCODE az64
	,sourcesystemtype VARCHAR(100)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (sourcesystemid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.sourcesystem owner to aqures01;