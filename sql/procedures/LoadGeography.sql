CREATE OR REPLACE PROCEDURE enabledw.LoadGeography()
	LANGUAGE plpgsql
AS $$

BEGIN
	 INSERT INTO enabledw.geography (city,county,state,postalcode,country ,createddate ,createdby ,modifieddate ,modifiedby )
		SELECT DISTINCT 
			city,
			'NA', 
			state, 
			postalcode, 
			country, 
			CURRENT_TIMESTAMP, 
			'System', 
			CURRENT_TIMESTAMP, 
			'System'
		FROM staging.stagingpatient s
	UNION
	 	SELECT DISTINCT 
	 		city, 
	 		'NA', 
	 		state, 
	 		postalcode, 
	 		country, 
	 		CURRENT_TIMESTAMP, 
	 		'System', 
	 		CURRENT_TIMESTAMP, 
	 		'System'
		FROM staging.stagingprovider p
	UNION
		SELECT DISTINCT 
			city, 
			'NA', 
			state, 
			postalcode, 
			country, 
			CURRENT_TIMESTAMP, 
			'System', 
			CURRENT_TIMESTAMP, 
			'System'
		FROM staging.stagingpractice pr; 
END;


$$
;
