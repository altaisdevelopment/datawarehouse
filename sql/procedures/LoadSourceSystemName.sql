CREATE OR REPLACE PROCEDURE enabledw.LoadSourceSystemName()
	LANGUAGE plpgsql
AS $$

BEGIN
	INSERT INTO enabledw.sourcesystem (sourcesystemtype,createddate ,createdby ,modifieddate ,modifiedby )
	SELECT DISTINCT 
		sourcesystemname, 
		CURRENT_TIMESTAMP, 
		'System', 
		CURRENT_TIMESTAMP, 
		'System'
	FROM staging.stagingpracticegroup pg; 
END;




$$
;