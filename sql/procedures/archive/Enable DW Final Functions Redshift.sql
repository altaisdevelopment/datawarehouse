TRUNCATE TABLE enabledw.age;
TRUNCATE TABLE enabledw.date;
TRUNCATE TABLE enabledw.gender;
TRUNCATE TABLE enabledw.geography ;
TRUNCATE TABLE enabledw.practicetype ;
TRUNCATE TABLE enabledw.providertype ;
TRUNCATE TABLE enabledw.sourcesystem ;
TRUNCATE TABLE enabledw.enablepatientfacts;
TRUNCATE TABLE enabledw.enableproviderfacts;
TRUNCATE TABLE enabledw.enablepracticefacts;


CALL enabledw.loadage(:f1,:f2);
CALL enabledw.loaddate(:f1,:f2);
CALL enabledw.loadgender(:f1,:f2);
CALL enabledw.loadgeography(:f1,:f2);
CALL enabledw.loadpracticetype(:f1,:f2);
CALL enabledw.loadprovidertype(:f1,:f2);
CALL enabledw.loadsourcesystemname(:f1,:f2);
CALL enabledw.loadenablepatientfacts(:f1,:f2);
CALL enabledw.loadenableproviders(:f1,:f2);
CALL enabledw.loadenablepractices(:f1,:f2);





CREATE OR REPLACE PROCEDURE enabledw.loadage(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.age (dob,yearsage ,monthsage,weeksage,daysage ,createddate ,createdby ,modifieddate ,modifiedby )
SELECT TO_DATE(s.dob,'YYMDD'),EXTRACT(YEAR  FROM TO_DATE(s.dob,'YYMDD') ), EXTRACT(MONTH  FROM TO_DATE(s.dob,'YYMDD') ), EXTRACT(WEEK  FROM TO_DATE(s.dob,'YYMDD') ),
EXTRACT(DAY  FROM TO_DATE(s.dob,'YYMDD') ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpatient s ;
END;







$$
;

CREATE OR REPLACE PROCEDURE enabledw.loaddate(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.date (yeardate ,monthdate ,quarterdate,weekdate,daydate ,createddate ,createdby ,modifieddate ,modifiedby )
SELECT DISTINCT EXTRACT(year FROM createddate ), EXTRACT(month FROM createddate ), EXTRACT(quarter FROM createddate ),EXTRACT(week FROM createddate ),
EXTRACT(day FROM createddate ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpatient s 
UNION
SELECT DISTINCT EXTRACT(year FROM createddate ), EXTRACT(month FROM createddate ), EXTRACT(quarter FROM createddate ),EXTRACT(week FROM createddate ),
EXTRACT(day FROM createddate ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingprovider p
UNION
SELECT DISTINCT EXTRACT(year FROM createddate ), EXTRACT(month FROM createddate ), EXTRACT(quarter FROM createddate ),EXTRACT(week FROM createddate ),
EXTRACT(day FROM createddate ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpractice pr 
UNION
SELECT DISTINCT EXTRACT(year FROM createddate ), EXTRACT(month FROM createddate ), EXTRACT(quarter FROM createddate ),EXTRACT(week FROM createddate ),
EXTRACT(day FROM createddate ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpracticegroup pg
;
END;


--SELECT DISTINCT CAST(createddate AS DATE) FROM staging.stagingprovider;






$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadenablepatientfacts(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	

DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.enablepatientfacts (totalpatients,geographyid,genderid,dateid,ageid,createddate ,createdby ,modifieddate ,modifiedby )
   -- sum of fact quant measures
   -- , ageid 
SELECT COUNT(S.patientid) AS PatientCount,

             ISNULL(G.geographyid, -1) AS GeographyID,

             ISNULL(GN.GenderID, -1) AS GenderID,

             ISNULL(D.DateID, -1) AS DateID,
			
             ISNULL(a.ageid, -1) AS AgeID,
CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'                          

       FROM staging.StagingPatient S

             LEFT JOIN Geography G

                    ON S.country = G.country

                           AND S.state = G.state

                          AND S.city = G.city

                          AND S.postalcode = G.postalcode

             LEFT JOIN Gender GN

                    ON S.gender = GN.gendertype
			LEFT JOIN age a
-- join on the actual date and the createddate/timestamp remove for the date
                    ON EXTRACT(YEAR  FROM TO_DATE(s.dob,'YYMDD') ) = a.yearsage

                    AND EXTRACT(MONTH FROM TO_DATE(s.dob,'YYMDD')) = a.monthsage

                    AND EXTRACT(DAY FROM TO_DATE(s.dob,'YYMDD') ) = a.daysage
                  
                      AND EXTRACT(WEEK FROM TO_DATE(s.dob,'YYMDD') ) = a.weeksage

                    
                    
             LEFT JOIN Date D

                    ON EXTRACT(YEAR  FROM s.createddate) = d.yeardate

                    AND EXTRACT(MONTH FROM s.createddate) = d.monthdate

                    AND EXTRACT(DAY FROM s.createddate) = d.daydate

       GROUP BY G.geographyid, GN.GenderID, D.DateID,a.ageid;
END;



$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadenablepractices(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
	

DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.enablepracticefacts (totalpractices,geographyid,dateid,practicetypeid ,createddate ,createdby ,modifieddate ,modifiedby )
   -- sum of fact quant measures
   -- , ageid 
  -- PRACTICE FACT
  SELECT COUNT(P.practiceid) AS PracticeCount, 
  	ISNULL(G.geographyid, -1) AS GeographyID,
 
                        ISNULL(D.DateID, -1) AS DateID,

  	ISNULL(PT.practicetypeid) AS PracticeTypeID,
  	CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
  FROM staging.StagingPractice P
  	LEFT JOIN Geography G 
  		ON P.country = G.country 
  			 AND P.state = G.state
  			AND P.city = G.city 
  			AND P.postalcode  = G.postalcode 


              LEFT JOIN Date D

                    ON EXTRACT(YEAR  FROM p.createddate) = d.yeardate

                    AND EXTRACT(MONTH FROM p.createddate) = d.monthdate

                    AND EXTRACT(DAY FROM p.createddate) = d.daydate

  -- taking out this for now until we decide to get this type in the table                    
 	LEFT JOIN PracticeType PT 
  		ON P.practicetype = PT.practicetype
  GROUP BY G.geographyid, PT.practicetypeid,d.dateid; 
  
END;




$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadenableproviders(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	

DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.enableproviderfacts (totalproviders,geographyid,genderid,ageid, dateid,createddate ,createdby ,modifieddate ,modifiedby )
   -- sum of fact quant measures
   -- , ageid 
  -- PROVIDER FACT
  SELECT COUNT(P.sourceproviderid) AS ProviderCount, 
  	ISNULL(G.geographyid, -1) AS GeographyID, 
           -1 AS GenderID, --  ISNULL(GN.GenderID, -1) AS GenderID,
-1 as AgeID, -- age id
             ISNULL(D.DateID, -1) AS DateID --,
			
           -- -1 -- ISNULL(a.ageid, -1) AS AgeID
  	,ISNULL(PT.providertypeid) AS ProviderTypeID,
  	CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
  	
  FROM staging.StagingProvider P
  	LEFT JOIN Geography G 
  		ON P.country = G.country 
  			 AND P.state = G.state
  			AND P.city = G.city 
  			AND P.postalcode  = G.postalcode 
  			
  		--	  	   LEFT JOIN Gender GN

          --          ON S.gender = GN.gendertype		
  	--		LEFT JOIN age a
-- join on the actual date and the createddate/timestamp remove for the date
      --              ON EXTRACT(YEAR  FROM TO_DATE(s.dob,'YYMDD') ) = a.yearsage

       --             AND EXTRACT(MONTH FROM TO_DATE(s.dob,'YYMDD')) = a.monthsage

         --           AND EXTRACT(DAY FROM TO_DATE(s.dob,'YYMDD') ) = a.daysage
        --          
         --             AND EXTRACT(WEEK FROM TO_DATE(s.dob,'YYMDD') ) = a.weeksage

              LEFT JOIN Date D

                    ON EXTRACT(YEAR  FROM P.createddate) = d.yeardate

                    AND EXTRACT(MONTH FROM P.createddate) = d.monthdate

                    AND EXTRACT(DAY FROM P.createddate) = d.daydate
  	LEFT JOIN ProviderType PT 
  	 	ON P.providertype = PT.providertype
  GROUP BY G.geographyid, PT.providertypeid,d.dateid; 
  
END;




$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadgender(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.gender (gendertype,birthsextype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT gender, birthsex,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingpatient p; 
END;



$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadgeography(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.geography (city,county,state,postalcode,country ,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT city,'NA',state,postalcode , country ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingpatient s
	UNION
 SELECT DISTINCT city,'NA',state,postalcode , country ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingprovider p
	UNION
 SELECT DISTINCT city,'NA',state,postalcode , country ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingpractice pr; 
END;





$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadpracticetype(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.practicetype (practicetype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT practicetype ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagepractice pr; 
END;


$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadprovidertype(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.providertype (providertype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT providertype,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stageprovider p; 
END;


$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadsourcesystemname(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.sourcesystem (sourcesystemtype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT sourcesystemname,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagepracticegroup pg; 
END;


$$
;
