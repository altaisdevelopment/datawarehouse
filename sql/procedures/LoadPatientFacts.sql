CREATE OR REPLACE PROCEDURE enabledw.LoadPatientFacts()
	LANGUAGE plpgsql
AS $$

BEGIN
  INSERT INTO enabledw.enablepatientfacts (totalpatients,geographyid,genderid,dateid,ageid,createddate ,createdby ,modifieddate ,modifiedby )
  SELECT 
  	COUNT(S.patientid) AS PatientCount, 
  	ISNULL(G.geographyid, -1) AS GeographyID,
    ISNULL(GN.GenderID, -1) AS GenderID,
    ISNULL(D.DateID, -1) AS DateID,
    ISNULL(a.ageid, -1) AS AgeID,
	CURRENT_TIMESTAMP , 
	'System', 
	CURRENT_TIMESTAMP, 
	'System'  
FROM staging.StagingPatient S
	LEFT JOIN enabledw.Geography G
		ON S.country = G.country
			AND S.state = G.state
            AND S.city = G.city
            AND S.postalcode = G.postalcode      

    LEFT JOIN enabledw.Gender GN
		ON S.gender = GN.gendertype 
           	AND S.birthsex = GN.birthsextype                    
                    
	LEFT JOIN enabledw.age a
		ON CAST(s.dob AS DATE) = a.dob                    
    
		
    LEFT JOIN enabledw.Date D
      	ON CAST(s.createddate AS DATE) = d.datefull             

GROUP BY G.geographyid, GN.GenderID, D.DateID,a.ageid;
END;


$$
;

