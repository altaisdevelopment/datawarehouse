CREATE OR REPLACE PROCEDURE enabledw.LoadGender()
	LANGUAGE plpgsql
AS $$
	

BEGIN
	INSERT INTO enabledw.gender 
		(gendertype,
		birthsextype,
		createddate,
		createdby,
		modifieddate,
		modifiedby) 
	SELECT DISTINCT 
		gender, 
		birthsex, 
		CURRENT_TIMESTAMP, 
		'System', 
		CURRENT_TIMESTAMP, 
		'System'
	FROM staging.stagingpatient p; 
END;


$$
;