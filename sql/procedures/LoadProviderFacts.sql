CREATE OR REPLACE PROCEDURE enabledw.LoadProviderFacts()
	LANGUAGE plpgsql
AS $$	

BEGIN
	
  INSERT INTO enabledw.enableproviderfacts (totalproviders,geographyid,genderid,ageid, dateid,providertypeid ,createddate ,createdby ,modifieddate ,modifiedby )
	SELECT 
		COUNT(P.sourceproviderid) AS ProviderCount, 
	  	ISNULL(G.geographyid, -1) AS GeographyID, 
	    -1 AS GenderID,
		-1 as AgeID, 
	    ISNULL(D.DateID, -1) AS DateID,
	  	ISNULL(PT.providertypeid) AS ProviderTypeID,
	  	CURRENT_TIMESTAMP,
	  	'System', 
	  	CURRENT_TIMESTAMP, 
	  	'System'
	FROM staging.StagingProvider P
	  	LEFT JOIN enabledw.Geography G 
	  		ON P.country = G.country 
	  			AND P.state = G.state
	  			AND P.city = G.city 
	  			AND P.postalcode  = G.postalcode 	  			
	  	LEFT JOIN enabledw.Date D
	      	ON CAST(P.createddate AS DATE) = d.datefull	      	
	  	LEFT JOIN enabledw.ProviderType PT 
	  	 	ON P.providertype = PT.providertype
	GROUP BY G.geographyid, PT.providertypeid,d.dateid;
  
END;

$$
;