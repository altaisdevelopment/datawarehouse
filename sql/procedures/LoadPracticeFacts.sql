CREATE OR REPLACE PROCEDURE enabledw.LoadPracticeFacts()
	LANGUAGE plpgsql
AS $$


BEGIN
  INSERT INTO enabledw.enablepracticefacts (totalpractices,geographyid,dateid,practicetypeid ,createddate ,createdby ,modifieddate ,modifiedby )
  SELECT 
  	COUNT(P.practiceid) AS PracticeCount, 
	ISNULL(G.geographyid, -1) AS GeographyID,
  	ISNULL(D.DateID, -1) AS DateID,
  	ISNULL(PT.practicetypeid) AS PracticeTypeID,
  	CURRENT_TIMESTAMP, 
  	'System', 
  	CURRENT_TIMESTAMP, 
  	'System'
  FROM staging.StagingPractice P
  	
  LEFT JOIN enabledw.Geography G 
  		ON P.country = G.country 
  			AND P.state = G.state
  			AND P.city = G.city 
  			AND P.postalcode  = G.postalcode 

	LEFT JOIN enabledw.Date D
      	ON CAST(P.createddate AS DATE) = d.datefull          
                  
 	LEFT JOIN enabledw.PracticeType PT 
  		ON P.practicetype = PT.practicetype
  GROUP BY G.geographyid, PT.practicetypeid,d.dateid; 
  
END;

$$
;