CREATE OR REPLACE PROCEDURE enabledw.loadage(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.age (yearsage ,monthsage,weeksage,daysage ,createddate ,createdby ,modifieddate ,modifiedby )
SELECT EXTRACT(YEAR  FROM TO_DATE(s.dob,'YYMDD') ), EXTRACT(MONTH  FROM TO_DATE(s.dob,'YYMDD') ), EXTRACT(WEEK  FROM TO_DATE(s.dob,'YYMDD') ),
EXTRACT(DAY  FROM TO_DATE(s.dob,'YYMDD') ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpatient s ;
END;






$$
;

CREATE OR REPLACE PROCEDURE enabledw.loaddate(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.date (yeardate ,monthdate ,quarterdate,weekdate,daydate ,createddate ,createdby ,modifieddate ,modifiedby )
SELECT DISTINCT EXTRACT(year FROM createddate ), EXTRACT(month FROM createddate ), EXTRACT(quarter FROM createddate ),EXTRACT(week FROM createddate ),
EXTRACT(day FROM createddate ),CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
FROM staging.stagingpatient s ;
END;






$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadenablepatientfacts(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	

DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.enablepatientfacts (totalpatients,geographyid,genderid,dateid,ageid,createddate ,createdby ,modifieddate ,modifiedby )
   -- sum of fact quant measures
   -- , ageid 
SELECT COUNT(S.patientid) AS PatientCount,

             ISNULL(G.geographyid, -1) AS GeographyID,

             ISNULL(GN.GenderID, -1) AS GenderID,

             ISNULL(D.DateID, -1) AS DateID,
			
             ISNULL(a.ageid, -1) AS AgeID
                          

       FROM staging.StagingPatient S

             LEFT JOIN Geography G

                    ON S.country = G.country

                          -- AND S.state = G.state

                          AND S.city = G.city

                          AND S.postalcode = G.postalcode

             LEFT JOIN Gender GN

                    ON S.gender = GN.gendertype
			LEFT JOIN age a
-- join on the actual date and the createddate/timestamp remove for the date
                    ON EXTRACT(YEAR  FROM TO_DATE(s.dob,'YYMDD') ) = a.yearsage

                    AND EXTRACT(MONTH FROM TO_DATE(s.dob,'YYMDD')) = a.monthsage

                    AND EXTRACT(DAY FROM TO_DATE(s.dob,'YYMDD') ) = a.daysage
                  
                      AND EXTRACT(WEEK FROM TO_DATE(s.dob,'YYMDD') ) = a.weeksage

                    
                    
             LEFT JOIN Date D

                    ON EXTRACT(YEAR  FROM s.createddate) = d.yeardate

                    AND EXTRACT(MONTH FROM s.createddate) = d.monthdate

                    AND EXTRACT(DAY FROM s.createddate) = d.daydate

       GROUP BY G.geographyid, GN.GenderID, D.DateID;
END;


$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadenablepractices(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	

DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.enablepracticefacts (totalpractices,geographyid,genderid,ageid, dateid,createddate ,createdby ,modifieddate ,modifiedby )
   -- sum of fact quant measures
   -- , ageid 
  -- PRACTICE FACT
  SELECT COUNT(P.practiceid) AS PracticeCount, 
  	ISNULL(G.geographyid, -1) AS GeographyID,
             ISNULL(GN.GenderID, -1) AS GenderID,

             ISNULL(D.DateID, -1) AS DateID,
			
             ISNULL(a.ageid, -1) AS AgeID

  	-- ,ISNULL(PT.practicetypeid) AS PracticeTypeID
  FROM staging.StagingPractice P
  	LEFT JOIN Geography G 
  		ON P.country = G.country 
  			-- AND P.state = G.state
  			AND P.city = G.city 
  			AND P.postalcode  = G.postalcode 
  	   LEFT JOIN Gender GN

                    ON S.gender = GN.gendertype		
  			LEFT JOIN age a
-- join on the actual date and the createddate/timestamp remove for the date
                    ON EXTRACT(YEAR  FROM TO_DATE(s.dob,'YYMDD') ) = a.yearsage

                    AND EXTRACT(MONTH FROM TO_DATE(s.dob,'YYMDD')) = a.monthsage

                    AND EXTRACT(DAY FROM TO_DATE(s.dob,'YYMDD') ) = a.daysage
                  
                      AND EXTRACT(WEEK FROM TO_DATE(s.dob,'YYMDD') ) = a.weeksage

              LEFT JOIN Date D

                    ON EXTRACT(YEAR  FROM s.createddate) = d.yeardate

                    AND EXTRACT(MONTH FROM s.createddate) = d.monthdate

                    AND EXTRACT(DAY FROM s.createddate) = d.daydate

  -- taking out this for now until we decide to get this type in the table                    
--  	LEFT JOIN PracticeType PT 
--  		ON P.practicetype = PT.practicetype
  GROUP BY G.geographyid, PT.practicetypeid; 
  
END;



$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadenableproviders(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	

DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.enableproviderfacts (totalproviders,geographyid,genderid,ageid, dateid,createddate ,createdby ,modifieddate ,modifiedby )
   -- sum of fact quant measures
   -- , ageid 
  -- PROVIDER FACT
  SELECT COUNT(P.sourceproviderid) AS ProviderCount, 
  	ISNULL(G.geographyid, -1) AS GeographyID, 
             ISNULL(GN.GenderID, -1) AS GenderID,

             ISNULL(D.DateID, -1) AS DateID,
			
             ISNULL(a.ageid, -1) AS AgeID
  	--,ISNULL(PT.providertypeid) AS ProviderTypeID
  	
  FROM staging.StagingProvider P
  	LEFT JOIN Geography G 
  		ON P.country = G.country 
  			-- AND P.state = G.state
  			AND P.city = G.city 
  			AND P.postalcode  = G.postalcode 
  			
  			  	   LEFT JOIN Gender GN

                    ON S.gender = GN.gendertype		
  			LEFT JOIN age a
-- join on the actual date and the createddate/timestamp remove for the date
                    ON EXTRACT(YEAR  FROM TO_DATE(s.dob,'YYMDD') ) = a.yearsage

                    AND EXTRACT(MONTH FROM TO_DATE(s.dob,'YYMDD')) = a.monthsage

                    AND EXTRACT(DAY FROM TO_DATE(s.dob,'YYMDD') ) = a.daysage
                  
                      AND EXTRACT(WEEK FROM TO_DATE(s.dob,'YYMDD') ) = a.weeksage

              LEFT JOIN Date D

                    ON EXTRACT(YEAR  FROM s.createddate) = d.yeardate

                    AND EXTRACT(MONTH FROM s.createddate) = d.monthdate

                    AND EXTRACT(DAY FROM s.createddate) = d.daydate
  	-- LEFT JOIN ProviderType PT 
  	-- 	ON P.providertype = PT.providertype
  GROUP BY G.geographyid, PT.providertypeid; 
  
END;



$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadgender(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.gender (gendertype,birthsextype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT gender, birthsex,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingpatient p; 
END;



$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadgeography(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.geography (city,county,state,postalcode,country ,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT city,'county',state,postalcode , country ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagingpatient s; 
END;



$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadpracticetype(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.practicetype (practicetype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT practicetype ,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagepractice pr; 
END;


$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadprovidertype(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.providertype (providertype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT providertype,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stageprovider p; 
END;


$$
;

CREATE OR REPLACE PROCEDURE enabledw.loadsourcesystemname(f1 varchar,f2 varchar)
	LANGUAGE plpgsql
AS $$
	
	
DECLARE
  error_val int;
BEGIN
  INSERT INTO enabledw.sourcesystem (sourcesystemtype,createddate ,createdby ,modifieddate ,modifiedby )
 SELECT DISTINCT sourcesystemname,CURRENT_TIMESTAMP ,'aqures01',CURRENT_TIMESTAMP ,'aqures01'
	FROM staging.stagepracticegroup pg; 
END;


$$
;
