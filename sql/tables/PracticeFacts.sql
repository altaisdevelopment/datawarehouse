CREATE TABLE IF NOT EXISTS enabledw.practicefacts
(
	practicefactid BIGINT NOT NULL DEFAULT "identity"(136938, 0, ('1,1'::character varying)::text) ENCODE az64
	,geographyid INTEGER NOT NULL  ENCODE az64
	,practicetypeid INTEGER   ENCODE az64
	,dateid INTEGER NOT NULL  ENCODE az64
	,totalpractices INTEGER NOT NULL  ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (practicefactid)
);