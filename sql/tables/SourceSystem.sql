CREATE TABLE IF NOT EXISTS enabledw.sourcesystem
(
	sourcesystemid BIGINT NOT NULL DEFAULT "identity"(118175, 0, ('1,1'::character varying)::text) ENCODE az64
	,sourcesystemtype VARCHAR(100)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (sourcesystemid)
);
