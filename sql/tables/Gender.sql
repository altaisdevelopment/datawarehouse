CREATE TABLE IF NOT EXISTS enabledw.gender
(
	genderid INTEGER NOT NULL DEFAULT default_identity(118214, 0, ('1,1'::character varying)::text) ENCODE az64
	,gendertype VARCHAR(100)   ENCODE lzo
	,birthsextype VARCHAR(100)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (genderid)
);
