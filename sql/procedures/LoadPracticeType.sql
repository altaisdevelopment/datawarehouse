CREATE OR REPLACE PROCEDURE enabledw.LoadPracticeType()
	LANGUAGE plpgsql
AS $$
	
BEGIN
	INSERT INTO enabledw.practicetype (practicetype,createddate ,createdby ,modifieddate ,modifiedby )
	SELECT DISTINCT 
		practicetype, 
		CURRENT_TIMESTAMP, 
		'System', 
		CURRENT_TIMESTAMP, 
		'System'
	FROM staging.stagingpractice pr; 
END;




$$
;
