CREATE TABLE IF NOT EXISTS enabledw.providertype
(
	providertypeid BIGINT NOT NULL DEFAULT "identity"(118160, 0, ('1,1'::character varying)::text) ENCODE az64
	,providertype VARCHAR(100)   ENCODE lzo
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (providertypeid)
);
