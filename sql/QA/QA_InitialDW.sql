/*
 
 -----------------------------------------------
 -- STAGING - EMPTY TABLES
 -----------------------------------------------
 
 TRUNCATE TABLE staging.stagingpatient;
 TRUNCATE TABLE staging.stagingprovider;
 TRUNCATE TABLE staging.stagingpractice;
 TRUNCATE TABLE staging.stagingpracticegroup;
  
  
  
-----------------------------------------------
-- ENABLE DW - EMPTY TABLES
-----------------------------------------------

-- CLEAR DIMENSION TABLES
TRUNCATE TABLE enabledw.age;
TRUNCATE TABLE enabledw.date;
TRUNCATE TABLE enabledw.gender;
TRUNCATE TABLE enabledw.geography ;
TRUNCATE TABLE enabledw.practicetype ;
TRUNCATE TABLE enabledw.providertype ;
TRUNCATE TABLE enabledw.sourcesystem ;


-- CLEAR FACT TABLES
TRUNCATE TABLE enabledw.enablepatientfacts;
TRUNCATE TABLE enabledw.enableproviderfacts;
TRUNCATE TABLE enabledw.enablepracticefacts;


*/


/*
-----------------------------------------------
-- LOAD ENABLE DW
-----------------------------------------------

-- POPULATE DIMENSIONS
CALL enabledw.LoadAge();
CALL enabledw.LoadDate();
CALL enabledw.LoadGender();
CALL enabledw.LoadGeography();
CALL enabledw.LoadPracticeType();
CALL enabledw.LoadProviderType();
CALL enabledw.LoadSourceSystemName();

-- POPULATE FACTS
CALL enabledw.LoadEnablePatientFacts();
CALL enabledw.LoadEnableProviders();
CALL enabledw.LoadEnablePractice();

*/


SELECT * FROM public.age;
SELECT * FROM enabledw.date;
SELECT * FROM enabledw.gender;
SELECT * FROM enabledw.geography ;
SELECT * FROM enabledw.practicetype ;
SELECT * FROM enabledw.providertype ;
SELECT * FROM enabledw.sourcesystem ;

SELECT COUNT(*) FROM enabledw.enablepatientfacts;
SELECT COUNT(*)FROM enabledw.enablepatientfacts;

SELECT COUNT(*) FROM enabledw.enableproviderfacts;
SELECT COUNT(*) FROM staging.stagingprovider s ;

SELECT * FROM staging.stagingprovider s ;
SELECT * FROM enabledw.enableproviderfacts;

SELECT COUNT(*) FROM enabledw.enablepracticefacts;
SELECT COUNT(*) FROM staging.stagingpractice s ;


SELECT * FROM enabledw.enablepatientfacts