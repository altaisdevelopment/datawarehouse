CREATE OR REPLACE PROCEDURE enabledw.LoadProviderType()
	LANGUAGE plpgsql
AS $$

BEGIN
	INSERT INTO enabledw.providertype (providertype, createddate, createdby, modifieddate, modifiedby )
	SELECT DISTINCT 
		providertype, 
		CURRENT_TIMESTAMP, 
		'System', 
		CURRENT_TIMESTAMP, 
		'System'
	FROM staging.stagingprovider p; 
END;




$$
;