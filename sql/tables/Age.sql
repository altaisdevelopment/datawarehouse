CREATE TABLE IF NOT EXISTS enabledw.age
(
	ageid BIGINT NOT NULL DEFAULT "identity"(136887, 0, ('1,1'::character varying)::text) ENCODE az64
	,dob DATE   ENCODE az64
	,yearsage INTEGER   ENCODE az64
	,monthsage INTEGER   ENCODE az64
	,weeksage INTEGER   ENCODE az64
	,daysage INTEGER   ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (ageid)
);