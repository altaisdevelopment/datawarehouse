-- enabledw.enablepatientfacts definition

-- Drop table

-- DROP TABLE enabledw.enablepatientfacts;

--DROP TABLE enabledw.enablepatientfacts;
CREATE TABLE IF NOT EXISTS enabledw.enablepatientfacts
(
	enablepatientfactid BIGINT NOT NULL DEFAULT "identity"(118318, 0, ('1,1'::character varying)::text) ENCODE az64
	,geographyid INTEGER NOT NULL  ENCODE az64
	,dateid INTEGER NOT NULL  ENCODE az64
	,genderid INTEGER NOT NULL  ENCODE az64
	,ageid INTEGER NOT NULL  ENCODE az64
	,totalpatients INTEGER NOT NULL  ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (enablepatientfactid)
)
DISTSTYLE AUTO
;
ALTER TABLE enabledw.enablepatientfacts owner to aqures01;