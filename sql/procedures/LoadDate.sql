CREATE OR REPLACE PROCEDURE enabledw.LoadDate()
	LANGUAGE plpgsql
AS $$
	
DECLARE
  error_val int;
BEGIN
	INSERT INTO enabledw.date (datefull, yeardate ,monthdate ,quarterdate,weekdate,daydate ,createddate ,createdby ,modifieddate ,modifiedby )
		SELECT DISTINCT 
			CAST(createddate AS DATE), 
			EXTRACT(year FROM createddate ), 
			EXTRACT(month FROM createddate ), 
			EXTRACT(quarter FROM createddate ), 
			EXTRACT(week FROM createddate ),
			EXTRACT(day FROM createddate), 
			CURRENT_TIMESTAMP, 
			'System', 
			CURRENT_TIMESTAMP, 
			'System'
		FROM staging.stagingpatient s 
	UNION
		SELECT DISTINCT 
			CAST(createddate AS DATE), 
			EXTRACT(year FROM createddate ), 
			EXTRACT(month FROM createddate ), 
			EXTRACT(quarter FROM createddate ), 
			EXTRACT(week FROM createddate ), 
			EXTRACT(day FROM createddate ), 
			CURRENT_TIMESTAMP, 
			'System', 
			CURRENT_TIMESTAMP, 
			'System'
		FROM staging.stagingprovider p
	UNION
		SELECT DISTINCT 
			CAST(createddate AS DATE), 
			EXTRACT(year FROM createddate ), 
			EXTRACT(month FROM createddate ), 
			EXTRACT(quarter FROM createddate ),
			EXTRACT(week FROM createddate ),
			EXTRACT(day FROM createddate ), 
			CURRENT_TIMESTAMP , 
			'System', 
			CURRENT_TIMESTAMP , 
			'System'
		FROM staging.stagingpractice pr 
	UNION
		SELECT DISTINCT 
			CAST(createddate AS DATE), 
			EXTRACT(year FROM createddate ), 
			EXTRACT(month FROM createddate ), 
			EXTRACT(quarter FROM createddate ), 
			EXTRACT(week FROM createddate ),
			EXTRACT(day FROM createddate ), 
			CURRENT_TIMESTAMP , 
			'System', 
			CURRENT_TIMESTAMP , 
			'System'
		FROM staging.stagingpracticegroup pg;
END;


$$
;