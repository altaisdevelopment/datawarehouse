CREATE TABLE IF NOT EXISTS enabledw.date
(
	dateid BIGINT NOT NULL DEFAULT "identity"(136893, 0, ('1,1'::character varying)::text) ENCODE az64
	,datefull DATE   ENCODE az64
	,yeardate INTEGER   ENCODE az64
	,monthdate INTEGER   ENCODE az64
	,quarterdate INTEGER   ENCODE az64
	,weekdate INTEGER   ENCODE az64
	,daydate INTEGER   ENCODE az64
	,createddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,createdby VARCHAR(20)   ENCODE lzo
	,modifieddate TIMESTAMP WITHOUT TIME ZONE   ENCODE az64
	,modifiedby VARCHAR(20)   ENCODE lzo
	,PRIMARY KEY (dateid)
);
